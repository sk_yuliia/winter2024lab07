import java.util.Random;
public class Deck{
	
	private Card[] cards;
	private int numOfCards;
	private Random rng;
	
	//Constructor
	public Deck(){
		this.numOfCards = 52;
		this.cards = new Card[numOfCards];
		String suits[] = {"Hearts", "Diamonds", "Spades", "Clubs"};
		String[] values = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "1"};
		//{"Two", "Three", "Four", "Five","Six", "Seven","Eight", "Nine", "Ten", "Jack", "Queen", "King", "Ace"};
		this.rng = new Random();
		
		int index = 0;
		for(int j = 0; j < suits.length; j++){
			for(int i = 0; i <values.length; i++){
				this.cards[index++] = new Card(suits[j], values[i]);
				}
			}
		}

	
	//Returns how many cards are on the deck 
	public int length(){
		return numOfCards;
	}
	
	//Returns the last position of the Card[] and reduces the numOfCards by 1 
	//If there's no cards left, it will return a NULL value
	public Card drawTopCard(){
		if(numOfCards <= 0){
			return null;
		} 
		numOfCards--;
		return cards[numOfCards];	
	}
	
	//Returns a String which is representation of all cards in the card[]
	public String toString(){
		String allCards = "";
		for(int i = 0; i < this.numOfCards; i++){
			allCards += this.cards[i] + "\n";
		}
		return allCards; 
	}
	
	//Changes the values of each index of the cards[] using random numbers
	public void shuffle(){
		for(int i = 0; i < this.numOfCards; i++){
			int randomNum = rng.nextInt(numOfCards - i) + i;
			Card temp = cards[i];
			cards[i] = cards[randomNum];
			cards[randomNum] = temp;
		}
	}
	
}
	
	
	
