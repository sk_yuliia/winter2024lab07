public class SimpleWar{
	public static void main(String[] args){
		Deck deck = new Deck();
		deck.shuffle();
		
		int player1 = 0;
		int player2 = 0;
		int round = 1;
		
		//Stops when there's no cards left in the deck
		while(deck.length() > 0){
		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		//Prints score of each player at the start of the round
		System.out.println("Player 1 score at the beginning of the round " + round + ": " + player1);
		System.out.println("Player 2 score at the beginning of the round " + round + ": " + player2);
		System.out.println("__________________________________");
		Card cardP1 = deck.drawTopCard();
		System.out.println("Player 1:" + cardP1 + "\n" + " Score: " + cardP1.calculateScore());
		
		Card cardP2 = deck.drawTopCard();
		System.out.println("Player 2:" + cardP2 + "\n" + " Score: " + cardP2.calculateScore());
		System.out.println("__________________________________");
		
		//Compares result of calculateScore in order to display who wins 
		if(cardP1.calculateScore() > cardP2.calculateScore()){
			System.out.println("Player 1 Wins!");
			player1++;
		} else if(cardP1.calculateScore() < cardP2.calculateScore()){
			System.out.println("Player 2 Wins!");
			player2++;
		} else {
			System.out.println("Both have same score");
		}
		//Prints score of each player at the end of the round
		System.out.println("Player 1 score at the end of the round " + round + ": " + player1);
		System.out.println("Player 2 score at the end of the round " + round + ": " + player2);
			round++;
		}
		//Prints result of the game
		System.out.println("\n" + "Result of the game: ");
		if(player1 > player2){
			System.out.println("Player 1 Wins! Congratulations!");
		} else if(player1 < player2){
			System.out.println("Player 2 Wins Congratulations!");
		} else {
			System.out.println("Nobody Wins.. Try again!");
		}
	}
	
}