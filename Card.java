public class Card{
	
	private String suit;
	private String value;
	
	//Constructor 
	public Card(String suit, String value){
		this.suit = suit;
		this.value = value;
	}
	
	//Getters 
	public String getSuit(){
		return this.suit;
	}
	public String getValue(){
		return this.value;
	}
	public String toString(){
		return this.value + " of " + this.suit;
	}
	//Calculates the value of the card and returns it
	public double calculateScore(){ 
		double decimalValue = Double.parseDouble(this.value);
		if(this.suit.equals("Hearts")){
			return decimalValue += 0.4;
		} else if(this.suit.equals("Diamonds")){
			return decimalValue += 0.3;
		} else if(this.suit.equals("Clubs")){
			 return decimalValue += 0.2;
		} else { 
			return decimalValue += 0.1;
		}
	}
}

